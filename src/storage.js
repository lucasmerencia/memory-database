class Storage {
    constructor(){
        this.stackedData = [{}];
    }
    
    getCurrent(){
        return this.stackedData[this.stackedData.length -1];
    }

    newLayer(){
        const current = this.getCurrent();
        const newData = {
            ...current
        }
        this.stackedData.push(newData);
    }

    discardLast(){
        if(this.stackedData.length <= 1) throw new Error('cannot discard root layer');
        this.stackedData.pop();
    }

    mergeLast(){
        if(this.stackedData.length <= 1) throw new Error('cannot merge root layer');
        const last = this.stackedData.pop();
        const current = this.stackedData.pop();
        const merged = {
            ...current,
            ...last
        }
        this.stackedData.push(merged);
    }
}

module.exports = Storage;