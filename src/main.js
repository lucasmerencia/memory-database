const readline = require('readline');
const Storage = require('./storage');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const storage = new Storage();
const CommandCreator = require('./command-creator');

const waitForCommand = () => {
    const commandCreator = new CommandCreator(storage);
    rl.question("> ", (command) => {
        const cmd = commandCreator.create(command);
        const response = cmd.exec();
        if(response){
            rl.write(response.toString());
            rl.write('\n');
        }
        waitForCommand();
    });
}

waitForCommand();