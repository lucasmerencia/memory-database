const commands = require('./commands');

const commandMapping = {
    'SET': commands.Set,
    'GET': commands.Get,
    'UNSET': commands.Unset,
    'NUMEQUALTO': commands.NumEqualTo,
    'END': commands.End,
    'BEGIN': commands.Begin,
    'ROLLBACK': commands.Rollback,
    'COMMIT': commands.Commit
}

class CommandCreator {
    constructor(data){
        this.data = data;
    }

    create(command){
        const commandParts = command.split(' ');
        const currentCommand = commandParts.shift();
        const Command = commandMapping[currentCommand.toUpperCase()];
        return new Command(this.data, ...commandParts);
    }
}

module.exports = CommandCreator;