const { assert } = require('chai');
const Storage = require('./storage');

describe('Storage', () => {
    let storage;

    beforeEach(() => {
        storage = new Storage();
    });

    describe('getCurrent', () => {
        it('returns the last layer', () => {
            storage.newLayer();
            storage.stackedData[1].key = 10;
            const data = storage.getCurrent();
            assert.equal(data.key, 10)
        })
    });

    describe('newLayer', () => {
        it('adds a new layer to the stack', () => {
            storage.newLayer();
            assert.lengthOf(storage.stackedData, 2);
        });

        it('adds a new layer cloning the last layer', () => {
            const data = storage.getCurrent();
            data.val1 = 10;
            storage.newLayer();
            assert.equal(storage.stackedData[1].val1, 10);
        })
    });

    describe('discardLast', () => {
        it('discards the last layer', () => {
            let data = storage.getCurrent();
            data.val1 = 10;
            storage.newLayer();
            data = storage.getCurrent();
            data.val2 = 20;
            storage.discardLast();
            data = storage.getCurrent();
            assert.lengthOf(storage.stackedData, 1);
            assert.isUndefined(data.val2);
            assert.equal(data.val1, 10);
        });

        it('does not discard the root layer', () => {
            try {
                storage.discardLast();
                assert.fail('must fail discarding root layer')
            } catch (error){
                assert.equal(error.message, 'cannot discard root layer');
            }
        });
    });

    describe('mergeLast', () => {
        it('merges the last layer', () => {
            let data = storage.getCurrent();
            data.val1 = 10;
            storage.newLayer();
            data = storage.getCurrent();
            data.val2 = 20;
            storage.mergeLast();
            data = storage.getCurrent();
            assert.lengthOf(storage.stackedData, 1);
            assert.equal(data.val2, 20);
            assert.equal(data.val1, 10);
        });

        it('does not discard the root layer', () => {
            try {
                storage.mergeLast();
                assert.fail('must fail merging root layer')
            } catch (error){
                assert.equal(error.message, 'cannot merge root layer');
            }
        })
    });
})
