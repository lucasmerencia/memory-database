const { assert } = require('chai');
const CommandCreator = require('./command-creator');
const commands = require('./commands');

describe('CommandCreator', () => {
    const creator = new CommandCreator();

    it('return Get commad', () => {
        const Command = creator.create('GET var');
        assert.instanceOf(Command, commands.Get);
    });

    it('return Set commad', () => {
        const Command = creator.create('SET var 10');
        assert.instanceOf(Command, commands.Set);
    });

    it('return Unset commad', () => {
        const Command = creator.create('UNSET var 10');
        assert.instanceOf(Command, commands.Unset);
    });

    it('return NumEqualTo commad', () => {
        const Command = creator.create('NUMEQUALTO 10');
        assert.instanceOf(Command, commands.NumEqualTo);
    });

    it('return End commad', () => {
        const Command = creator.create('END');
        assert.instanceOf(Command, commands.End);
    });

    it('return Begin commad', () => {
        const Command = creator.create('BEGIN');
        assert.instanceOf(Command, commands.Begin);
    });

    it('return Rollback commad', () => {
        const Command = creator.create('ROLLBACK');
        assert.instanceOf(Command, commands.Rollback);
    });

    it('return Commit commad', () => {
        const Command = creator.create('COMMIT');
        assert.instanceOf(Command, commands.Commit);
    });
});