class Get {
    constructor(storage, name){
        this.storage = storage;
        this.name = name;
    }

    exec(){
        const data = this.storage.getCurrent();
        return data[this.name] ?? 'NULL';
    }
}

module.exports = Get;