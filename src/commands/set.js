class Set {
    constructor(storage, name, value){
        this.storage = storage;
        this.name = name;
        this.value = value;
    }

    exec(){
        const data = this.storage.getCurrent();
        data[this.name] = this.value;
    }
}

module.exports = Set;