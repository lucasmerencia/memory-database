class Begin {
    constructor(storage){
        this.storage = storage;
    }
    exec(){
        this.storage.newLayer();
    }
}

module.exports = Begin;