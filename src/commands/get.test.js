const { assert } = require('chai');
const Get = require('./get');
const Storage = require('../storage');

describe('Get', () => {
    const storage = new Storage();
    
    it('should get a value from memory data', () => {
        const get = new Get(storage, 'key');
        storage.getCurrent().key = 5;
        const value = get.exec();
        assert.equal(value, 5);
    });

    it('should return null when value is not defined', () => {
        const get = new Get(storage, 'some key');
        const value = get.exec();
        assert.equal(value, 'NULL');
    });
});