const { assert } = require('chai');
const Rollback = require('./rollback');
const Storage = require('../storage');

describe('Rollback', () => {
    const storage = new Storage();
    
    it('should discard last layer', () => {
        const rollback = new Rollback(storage);
        storage.getCurrent().key = 5;
        storage.newLayer();
        storage.getCurrent().key = 10;
        storage.getCurrent().var = 1;
        rollback.exec();
        assert.lengthOf(storage.stackedData, 1);
        assert.equal(storage.getCurrent().key, 5);
        assert.isUndefined(storage.getCurrent().var);
    });

    it('handles discarging root layer', () => {
        const rollback = new Rollback(storage);
        const response = rollback.exec();
        assert.equal(response, 'NO TRANSACTION');
    });
});