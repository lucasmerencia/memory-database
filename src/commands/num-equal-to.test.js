const { assert } = require('chai');
const NumEqualTo = require('./num-equal-to');
const Storage = require('../storage');

describe('NumEqualTo', () => {
    const storage = new Storage();
    const data = storage.getCurrent();
    data.var1 = 10;
    data.var2 = 10;
    data.var3 = 5;

    const numEqualTo = new NumEqualTo(storage, 10);
    
    it('should count the number of variables with a value', () => {
        const count = numEqualTo.exec();
        assert.equal(count, 2);
    });
});