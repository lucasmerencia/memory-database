class NumEqualTo {
    constructor(storage, value){
        this.storage = storage;
        this.value = value;
    }

    exec(){
        const data = this.storage.getCurrent();
        return Object.keys(data)
            .map(key => data[key])
            .filter(val => val === this.value).length;
    }
}

module.exports = NumEqualTo;