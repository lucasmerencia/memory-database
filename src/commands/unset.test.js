const { assert } = require('chai');
const Unset = require('./unset');
const Storage = require('../storage');

describe('Unset', () => {
    const storage = new Storage();

    it('should unset a variable', () => {
        storage.getCurrent().key = 5;
        const unset = new Unset(storage, 'key');
        unset.exec();
        assert.isUndefined(storage.getCurrent().key);
    });
});