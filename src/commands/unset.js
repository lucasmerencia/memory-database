class Unset {
    constructor(storage, name){
        this.storage = storage;
        this.name = name;
    }

    exec(){
        const data = this.storage.getCurrent();
        delete data[this.name];
    }
}

module.exports = Unset;