const { assert } = require('chai');
const End = require('./end');

describe('End', () => {
    const data = {};
    const end = new End(data);

    it('exits the process with code 0', () => {
        const exitFn = process.exit;
        let receivedCode;
        process.exit = (code) => {
            receivedCode = code;
        } 
        end.exec();
        
        assert.equal(receivedCode, 0);
        process.exit = exitFn;
    });
});