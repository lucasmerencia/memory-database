const { assert } = require('chai');
const Set = require('./set');
const Storage = require('../storage');

describe('Set', () => {
    const storage = new Storage();

    it('should set a value to memory data', () => {
        const set = new Set(storage, 'key', 10);
        set.exec();
        assert.equal(storage.getCurrent().key, 10);
    });
});