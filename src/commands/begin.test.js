const { assert } = require('chai');
const Begin = require('./begin');
const Storage = require('../storage');

describe('Begin', () => {
    const storage = new Storage();
    
    it('should create a new layer of data', () => {
        const begin = new Begin(storage);
        storage.getCurrent().key = 5;
        begin.exec();
        assert.lengthOf(storage.stackedData, 2);
        assert.equal(storage.stackedData[0].key, 5);
        assert.equal(storage.stackedData[1].key, 5);
    });
});