const { assert } = require('chai');
const Commit = require('./commit');
const Storage = require('../storage');

describe('Commit', () => {
    let storage;
    
    beforeEach(() => {
        storage = new Storage();
    });

    it('merges the last layer', () => {
        const commit = new Commit(storage);
        storage.getCurrent().key = 5;
        storage.newLayer();
        storage.getCurrent().key = 10;
        storage.getCurrent().var = 1;
        commit.exec();
        assert.lengthOf(storage.stackedData, 1);
        assert.equal(storage.getCurrent().key, 10);
        assert.equal(storage.getCurrent().var, 1);
    });

    it('handles merging root layer', () => {
        const commit = new Commit(storage);
        const response = commit.exec();
        assert.equal(response, 'NO TRANSACTION');
    });
});