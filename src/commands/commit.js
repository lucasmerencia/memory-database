class Commit{
    constructor(storage){
        this.storage = storage;
    }

    exec(){
        try{
            this.storage.mergeLast();
        } catch(error){
            if(error.message === 'cannot merge root layer') return 'NO TRANSACTION';
            else throw error;
        }
    }
}
module.exports = Commit;