class Rollback {
    constructor(storage){
        this.storage = storage;
    }
    exec(){
        try{
            this.storage.discardLast();
        } catch(error){
            if(error.message === 'cannot discard root layer') return 'NO TRANSACTION';
            else throw error;
        }
    }
}

module.exports = Rollback;